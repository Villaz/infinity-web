FROM ubuntu:latest
MAINTAINER luis.villazon.esteban@cern.ch

RUN apt-get -y update
RUN apt-get install -y wget
RUN apt-get install -y git
RUN wget http://nodejs.org/dist/v0.12.3/node-v0.12.3-linux-x64.tar.gz
RUN tar -zxvf node-v0.12.3-linux-x64.tar.gz
RUN cp -R node-v0.12.3-linux-x64/* /usr/
RUN rm -rf node-v0.12.3-linux-x64
RUN npm -g install bower
RUN apt-get install -y python

WORKDIR /root
RUN git clone https://bitbucket.org/Villaz/infinity-web.git
WORKDIR /root/infinity-web
RUN npm install node-pre-gyp -g
RUN npm install
RUN bower install  --allow-root jquery-colorbox

#ADD infinity.conf /etc/infinity/
VOLUME /etc/infinity
CMD npm start
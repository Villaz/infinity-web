var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var Promise = require('bluebird');
var fs = require("fs");
var request = require("request");
var influx = require("influx");
var moment = require("moment");

function connect(){
  return new Promise(function(resolve,reject){
    var url = 'mongodb://luis:Espronceda@ds047911.mongolab.com:47911/infinity';
    MongoClient.connect(url, function(err, db) {
      resolve(db.collection('servers'));
    });
  });
}


function get1minFromVM(name){
  var client = influx({
    host : '52.24.246.168',
    port : 8086, // optional, default 8086
    protocol : 'http', // optional, default 'http'
    username : 'root',
    password : 'Espronceda',
    database : 'servers'
  });
  
  var query = "SELECT value FROM load1 where hostname = '"+name+"'";
  return new Promise(function(resolve,reject){
    client.query(query,function(err,data){
      var points = [];
      var times = [];
      if(!err)
        if(data[0] === undefined)
        {
          resolve([[],[]]);
          return;
        }
        for(var i in data[0].points){
          times.push(moment.unix(data[0].points[i][0]).format("h:mm:ss"));
          points.push(data[0].points[i][2]);
        }
        
      var t = "";
      times = times.reverse();
      var first = true;
      for(var i in times){
        if(!first)
          t+=",";
        t += "'"+times[i]+"'";
        first = false;
      }
      resolve([points.reverse(),t]);
    });
});
  
}

function loadFiles(url){
  return new Promise(function(resolve,reject){
    request(url,function (error, response, body) {
      if(error) reject(error);
      resolve(body);
    });
  });
  
}

function loadSites(){
  var info = JSON.parse(fs.readFileSync('/etc/infinity/infinity.conf'));
  var sites = [];
  for(var site in info['universe']['experiments']['ATLAS'])
    sites.push({name:site});
  return sites;
}


var db = connect();

/* GET home page. */
router.get('/', function(req, res, next) {
  // Connection URL
  db.then(function(collection){
    collection.find({},{
      "limit": 20,
      "skip": 20,
      "sort": "createdTime"
    }).toArray(function(err, docs) {
      res.render('index', { title: 'Express' ,
      sites: loadSites(),
      total:docs.length,
      //nextpage:id+1,
      //lastpage:id-1,
      docs: docs,
      helpers:{
        ifCond:function(v1, v2 , options) {
            if(v1 === v2) {
              return options.fn(this);
            }else {
              return options.inverse(this);
          }
        }
      }
      });
    });
  });

});


router.get("/list/:id?", function(req, res, next) {
  // Connection URL
  db.then(function(collection){
    collection.find({site:'FZJ'},{
      "limit": 20,
      "skip": 0,
      "sort": "createdTime"
    }).toArray(function(err, docs) {
      res.render('index', { title: 'Express' ,
      total:docs.length,
      nextpage:1,
      lastpage:-1,
      docs: docs,
      helpers:{
        ifCond:function(v1, v2 , options) {
            if(v1 === v2) {
              return options.fn(this);
            }else {
              return options.inverse(this);
          }
        }
      }
      });
    });
  });
});


router.get("/table",function(req,res,next){
  db.then(function(collection){
    var search = {};
    search = {$and:[{hostname:{$exists:true}}]};
    if(req.query.search !== undefined)
      search['$and'].push({hostname:{$regex :".*"+req.query.search+".*"}});
    if(req.query.site !== undefined)
      search.site = req.query.site.toUpperCase();
    if(req.query.state !== undefined)
      search.state = req.query.state.toUpperCase();

    collection.find(search, {limit:20, skip:0 , sort:'createdTime'})
    .toArray(function(err, docs) {
      if(err) console.log(err)
      res.render('parcials/table_servers', {
      layout:'ajax',
      total:docs.length,
      nextpage:1,
      lastpage:-1,
      docs: docs,
      helpers:{
        ifCond:function(v1, v2 , options) {
           if(v1 === v2) {
              return options.fn(this);
            }else {
              return options.inverse(this);
          }
        }
      }
      });
    });
  });
});

router.get('/logs/:id',function(req,res,next){
  db.then(function(collection){
    var search = {};
    search = {hostname:req.params.id};
    collection.find(search).toArray(function(err,docs){
     var values = {};
     
     for(var i in docs[0].s3){
       if(docs[0].s3[i].indexOf('vm') >= 0){
         values[docs[0].s3[i].substr(docs[0].s3[i].lastIndexOf("/")+1)] = loadFiles(docs[0].s3[i]);
       }
     }
     Promise.props(values).then(function(a){
       var docs = []
       for(var i in a){
         docs.push({name:i,data:a[i]});
       }
       res.render('parcials/logs',{layout:'ajax',docs:docs}); 
     });
     
    });
  });
});

router.get('/map', function(req, res, next){
  res.render('map',{layout:'map'})
});

router.get('/chart/:vm',function(req, res, next){
  get1minFromVM(req.params.vm).then(function(points){
    res.render('parcials/areaChart',
    {
      vm:req.params.vm,
      points:JSON.stringify(points[0]),times:points[1],
      layout:'ajax'
    });
  });
  
})

module.exports = router;

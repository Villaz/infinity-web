var MongoClient = require('mongodb').MongoClient;

var listen = function (site, callback, callback_end){
  var url = 'mongodb://luis:Espronceda@54.87.228.58:47911/infinity';
  MongoClient.connect(url, function(err, db) {
    if(err){
      console.error(err)
      callback_end()
      return
    }
    var coll = db.collection('capped');
      var doc = {};
      if(site !== 'All')
        doc.site = site;
      var lastElement = coll.find(doc).sort({$natural:-1}).limit(1);
      var lastId = undefined;
      lastElement.nextObject(function(err, latest) {
        if(latest == null)
        {
          callback_end();
          return;
        }
        lastId = latest._id;

        var options = {
          tailable: true,
          awaitdata: true,
          sort: {$natural: 1},
          exhaust:true
        };

        var latestCursor = coll.find({_id:{$gt:lastId},site:site},options).stream();
        latestCursor.on('data',function(data){
          callback(site, data);
        });
        latestCursor.on('end',callback_end);
    });
  });
 };


var test = function(site , callback, callback_end){

  setInterval(function(){callback(site,{state:'ENDED',hostname:'Hola'});},2000);
};

  module.exports = {notice:listen};
